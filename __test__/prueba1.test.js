const {resta, suma} = require('../src/calculadora');

let contador = 1;
let users = ["Vanessa", "Marco", "Luis", "Ana", "Hector"];
let user = {
    name: "Hector",
    lastname: "Rodriguez",
    email: "hector02@gmail.com"
};

describe("Calculos matematicos", () => {
    test("Suma 1 + 2", () => {
        expect(suma(1,2)).toBe(3); // (1+2) === 3
    });

    it("Resta 5-2", () => {
        expect(resta(5,2)).toBe(3);  // (5-2) === 3
    });

    // !==
    it("Multiplicacion 5*2 != 11", () => {
        expect(5*2).not.toBe(11); // (5*2) !== 11
    });

    //toEqual para comparar arreglos
    it("Comparando arreglos", () => {
        expect([1,2,3]).toEqual([1,2,3]);
    });

    //toEqual para comparar objetos
    it("Comparando objetos", () => {
        expect({name: "Luis", lastname: "Torres"}).toEqual({name: "Luis", lastname: "Torres"});
    });

    it("Comprobando que exista Luis dentro del arreglo de usuarios", () => {
        expect(users).toContain("Luis");
    });

    it("Comprobando que exista Luis, Marco dentro del arreglo de usuarios", () => {
        expect(users).toEqual(expect.arrayContaining(["Luis", "Marco"]))
    });

    it("Comprobando que el objeto de user tenga como propiedad name y lastname", () => {
        // expect(user).toEqual(expect.objectContaining({
        //     name: expect.any(String),
        //     lastname: expect.any(String),
        //     email: expect.any(String)
        // }));
        expect(user).toHaveProperty("name", "Hector");
        expect(user).toHaveProperty("lastname", "Rodriguez");
    });

    beforeAll(() => {
        console.log("Corriendo la suit de pruebas para los calculos matematicos");
    });

    beforeEach(() => {
        console.log("Prueba no.", contador);
        contador++;
    });

    afterAll(() => {
        console.log("Borrando los registros usados para pruebas");
    })

    afterEach(() => {
        console.log("Cerrando sesión del usuario");
    })
});